package com.mygdx.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.badlogic.gdx.utils.viewport.Viewport;

public class Singleton {

    private static Singleton instance = null;

    public Integer leftPoints, rightPoints;
    public OrthographicCamera camera;
    public Viewport viewport;

    private Singleton() {
        leftPoints = 0;
        rightPoints = 0;

        camera = new OrthographicCamera(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        viewport = new FitViewport(Pong.WIDTH, Pong.HEIGHT, camera);
        viewport.apply();
        camera.translate(camera.viewportWidth / 2, camera.viewportHeight / 2);
    }

    public static Singleton getInstance() {
        if (instance == null) {
            instance = new Singleton();
        }
        return instance;
    }

    public void resetPoints() {
        leftPoints = 0;
        rightPoints = 0;
    }
}
