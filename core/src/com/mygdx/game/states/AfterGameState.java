package com.mygdx.game.states;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.mygdx.game.Pong;
import com.mygdx.game.Singleton;

public class AfterGameState extends State {

    private final GameStateManager gsm;

    private final BitmapFont leftPointsFont, rightPointsFont, winningFont, replayFont;
    private final String winningText;
    private static final String REPLAYTEXT = "Replay!";

    private static Texture board;
    private final Texture replayButton;

    public AfterGameState(GameStateManager gsm) {
        super(gsm);

        this.gsm = gsm;

        leftPointsFont = new BitmapFont();
        leftPointsFont.getData().setScale(3);
        leftPointsFont.setColor(Color.YELLOW);
        rightPointsFont = new BitmapFont();
        rightPointsFont.getData().setScale(3);
        rightPointsFont.setColor(Color.YELLOW);

        winningFont = new BitmapFont();
        winningFont.getData().setScale(3);
        winningFont.setColor(Color.BLACK);
        winningText = (Singleton.getInstance().leftPoints == Pong.POINTSTOWIN)? "Left player won!" : "Right player won!";

        replayFont = new BitmapFont();
        replayFont.getData().setScale(3);
        replayFont.setColor(Color.YELLOW);

        board = new Texture("PongboardWPaddles.png");
        replayButton = new Texture("Playbutton.png");
    }

    @Override
    protected void handleInput() {
        if (Gdx.input.justTouched() || Gdx.input.isKeyPressed(Input.Keys.ENTER) || Gdx.input.isKeyPressed(Input.Keys.SPACE)) {
            Singleton.getInstance().resetPoints();
            gsm.set(new PlayState(gsm));
        }
    }

    @Override
    public void update(float dt) {
        handleInput();
    }

    @Override
    public void render(SpriteBatch sb) {
        // Render board
        sb.draw(board, 0, 0);

        // Render points
        Integer leftPoints = Singleton.getInstance().leftPoints;
        Integer rightPoints = Singleton.getInstance().rightPoints;

        GlyphLayout layout = new GlyphLayout(leftPointsFont, leftPoints.toString());
        leftPointsFont.draw(sb, leftPoints.toString(), Pong.WIDTH / 2.0f - 40 - layout.width, Pong.HEIGHT - 28);
        rightPointsFont.draw(sb, rightPoints.toString(), Pong.WIDTH / 2.0f + 40, Pong.HEIGHT - 28);

        // Render who won
        layout = new GlyphLayout(winningFont, winningText);
        winningFont.draw(sb, winningText, Pong.WIDTH / 2.0f - layout.width / 2.0f, Pong.HEIGHT - (Pong.HEIGHT / 4.0f) + layout.height);

        // Render replay-button and replay-text
        sb.draw(replayButton, Pong.WIDTH / 2.0f - replayButton.getWidth() / 2.0f + 25, Pong.HEIGHT / 2.0f - replayButton.getHeight() / 2.0f);
        layout = new GlyphLayout(replayFont, REPLAYTEXT);
        replayFont.draw(sb, REPLAYTEXT, Pong.WIDTH / 2.0f - layout.width / 2.0f, Pong.HEIGHT / 2.0f + layout.height / 2);
    }

    @Override
    public void dispose() {
        board.dispose();
        replayButton.dispose();
    }
}
