package com.mygdx.game.states;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.mygdx.game.Pong;
import com.mygdx.game.Singleton;
import com.mygdx.game.states.sprites.Ball;
import com.mygdx.game.states.sprites.Paddle;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class PlayState extends State {

    private final Paddle leftPaddle;
    private final Paddle rightPaddle;
    private final Ball ball;

    private final BitmapFont leftPointsFont, rightPointsFont, vFont;

    private final Texture board;
    private final Sound ballPaddleSound;
    private float ballPaddleAudioLevel;
    private final static float AUDIOLEVELINCREMENT = 0.04f;

    protected PlayState(GameStateManager gsm) {
        super(gsm);

        // Game objects
        leftPaddle = new Paddle(25);
        rightPaddle = new Paddle(Pong.WIDTH - 25);
        ball = new Ball();

        // Text
        leftPointsFont = new BitmapFont();
        leftPointsFont.getData().setScale(3);
        leftPointsFont.setColor(Color.YELLOW);
        rightPointsFont = new BitmapFont();
        rightPointsFont.getData().setScale(3);
        rightPointsFont.setColor(Color.YELLOW);

        vFont = new BitmapFont();
        vFont.getData().setScale(1);
        vFont.setColor(Color.YELLOW);

        // Audio
        board = new Texture("Pongboard.png");
        ballPaddleSound = Gdx.audio.newSound(Gdx.files.internal("BallPaddleHit.mp3"));
        ballPaddleAudioLevel = 1;
    }

    @Override
    protected void handleInput() {
        // If screen is clicked/touched
        if(Gdx.input.isTouched()) {
            if (Gdx.input.isTouched(0)) {
                handleTouch(0);
            }
            if (Gdx.input.isTouched(1)) {
                handleTouch(1);
            }
            // Screen is not touched, check if keys are pressed
        } else {
            // If left player keys pressed
            if (Gdx.input.isKeyPressed(Input.Keys.W)) {
                if (leftPaddle.getPosition().y + leftPaddle.getTexture().getHeight() < Pong.HEIGHT - Pong.WALLHEIGHT) {
                    leftPaddle.moveTo(leftPaddle.getPosition().y + 5);
                }
            } else if (Gdx.input.isKeyPressed(Input.Keys.S)) {
                if (leftPaddle.getPosition().y > Pong.WALLHEIGHT) {
                    leftPaddle.moveTo(leftPaddle.getPosition().y - 5);
                }
            }
            // If right player keys are pressed
            if (Gdx.input.isKeyPressed(Input.Keys.UP)) {
                if (rightPaddle.getPosition().y + rightPaddle.getTexture().getHeight() < Pong.HEIGHT - Pong.WALLHEIGHT) {
                    rightPaddle.moveTo(rightPaddle.getPosition().y + 5);
                }
            } else if (Gdx.input.isKeyPressed(Input.Keys.DOWN)) {
                if (rightPaddle.getPosition().y > Pong.WALLHEIGHT) {
                    rightPaddle.moveTo(rightPaddle.getPosition().y - 5);
                }
            }
        }
    }

    protected void handleTouch(int pointer) {
        // Read coordinates
        Vector3 coordinates = new Vector3(Gdx.input.getX(pointer), Gdx.input.getY(pointer), 0);
        Viewport viewport = Singleton.getInstance().viewport;
        Singleton.getInstance().camera.unproject(coordinates, viewport.getScreenX(), viewport.getScreenY(), viewport.getScreenWidth(), viewport.getScreenHeight());

        // Choose what paddle to move
        Paddle activePaddle;
        if (coordinates.x < Pong.WIDTH / 2.0f) {
            activePaddle = leftPaddle;
        } else {
            activePaddle = rightPaddle;
        }

        // If touch is higher than the middle of the paddle
        if (coordinates.y > activePaddle.getPosition().y + activePaddle.getTexture().getHeight() / 2.0f) {
            if (activePaddle.getPosition().y + activePaddle.getTexture().getHeight() < Pong.HEIGHT - Pong.WALLHEIGHT) {
                if (Math.abs((activePaddle.getPosition().y + activePaddle.getTexture().getHeight() / 2.0f) - coordinates.y) < 5) {
                    activePaddle.moveTo(coordinates.y - activePaddle.getTexture().getHeight() / 2.0f);
                } else {
                    activePaddle.moveTo(activePaddle.getPosition().y + 5);
                }
            }
            // Touch is lower than the middle of the paddle
        } else {
            if (activePaddle.getPosition().y > Pong.WALLHEIGHT) {
                if (activePaddle.getPosition().y + activePaddle.getTexture().getHeight() / 2.0f - coordinates.y < 5) {
                    activePaddle.moveTo(coordinates.y - activePaddle.getTexture().getHeight() / 2.0f);
                } else {
                    activePaddle.moveTo(activePaddle.getPosition().y - 5);
                }
            }
        }
    }

    @Override
    public void update(float dt) {
        handleInput();
        ball.update();
    }

    @Override
    public void render(SpriteBatch sb) {
        // Render board
        sb.draw(board, 0, 0);

        // Render points
        GlyphLayout leftPointsLayout = new GlyphLayout(leftPointsFont, Singleton.getInstance().leftPoints.toString());
        leftPointsFont.draw(sb, Singleton.getInstance().leftPoints.toString(), Pong.WIDTH / 2.0f - 40 - leftPointsLayout.width, Pong.HEIGHT - 28);
        rightPointsFont.draw(sb, Singleton.getInstance().rightPoints.toString(), Pong.WIDTH / 2.0f + 40, Pong.HEIGHT - 28);

        // Get coordinates of ball
        float ballXPos = ball.getPosition().x;
        float ballYPos = ball.getPosition().y;

        // Check if ball is at top/bottom edges or at a paddle
        checkBallReverseYAxis(ballYPos);
        checkBallReverseXAxis(ballXPos, ballYPos);

        // If left player won, right player won or game should proceed
        if(leftPlayerWon(ballXPos)) {
            if (++Singleton.getInstance().leftPoints == Pong.POINTSTOWIN) {
                gsm.set(new AfterGameState(gsm));
            } else {
                gsm.set(new AfterRoundState(gsm));
            }
        } else if (rightPlayerWon(ballXPos)) {
            if (++Singleton.getInstance().rightPoints == Pong.POINTSTOWIN) {
                gsm.set(new AfterGameState(gsm));
            } else {
                gsm.set(new AfterRoundState(gsm));
            }
        } else {
            // Render ball and paddles
            sb.draw(ball.getTexture(), ball.getPosition().x, ball.getPosition().y);
            sb.draw(leftPaddle.getTexture(), leftPaddle.getPosition().x, leftPaddle.getPosition().y);
            sb.draw(rightPaddle.getTexture(), rightPaddle.getPosition().x, rightPaddle.getPosition().y);

            // Calculate and render velocity of ball
            double velocity = BigDecimal.valueOf(Math.sqrt(Math.pow(ball.getVelocity().x, 2) + Math.pow(ball.getVelocity().y, 2))).setScale(1, RoundingMode.CEILING).doubleValue();
            vFont.draw(sb, "V : " + velocity, 10, Pong.HEIGHT - 10 - Pong.WALLHEIGHT);
        }
    }

    public void checkBallReverseYAxis(float ballYPos) {
        // Reverse ball-direction on y-axis if at top/bottom wall
        if (ballYPos + ball.getTexture().getHeight() > Pong.HEIGHT - Pong.WALLHEIGHT || ballYPos < Pong.WALLHEIGHT) {
            ball.reverseYVelocity();
        }
    }

    public void checkBallReverseXAxis(float ballXPos, float ballYPos) {
        // Reverse ball-direction on x-axis if at left paddle
        if (ballXPos >= leftPaddle.getPosition().x + leftPaddle.getTexture().getWidth() - (Math.abs(ball.getVelocity().x) + 1) && ballXPos <= leftPaddle.getPosition().x + leftPaddle.getTexture().getWidth()) {
            if (ballYPos >= leftPaddle.getPosition().y && ballYPos <= leftPaddle.getPosition().y + leftPaddle.getTexture().getHeight()) {
                if (ballXPos < Pong.WIDTH / 2.0f && ball.getVelocity().x < 0) {
                    ball.reverseXVelocity();
                    playBallPaddleHitSound();
                }
            }
            // Reverse ball-direction on x-axis if at right paddle
        } else if (ballXPos + ball.getTexture().getWidth() >= rightPaddle.getPosition().x && ballXPos <= rightPaddle.getPosition().x + (Math.abs(ball.getVelocity().y) + 1)) {
            if (ballYPos >= rightPaddle.getPosition().y && ballYPos <= rightPaddle.getPosition().y + rightPaddle.getTexture().getHeight()) {
                if (ballXPos > Pong.WIDTH / 2.0f && ball.getVelocity().x > 0) {
                    ball.reverseXVelocity();
                    playBallPaddleHitSound();
                }
            }
        }
    }

    public boolean leftPlayerWon(float ballXPos) {
        return ballXPos > rightPaddle.getPosition().x + rightPaddle.getTexture().getWidth() / 2.0f + 80;
    }

    public boolean rightPlayerWon(float ballXPos) {
        return ballXPos < leftPaddle.getPosition().x - 80;
    }

    public void playBallPaddleHitSound() {
        long id = ballPaddleSound.play();
        ballPaddleSound.setVolume(id, ballPaddleAudioLevel);
        ballPaddleSound.setPitch(id, ballPaddleAudioLevel);
        ballPaddleAudioLevel += AUDIOLEVELINCREMENT;
    }

    @Override
    public void dispose() {
        leftPaddle.dispose();
        rightPaddle.dispose();
        ball.dispose();
        board.dispose();
        ballPaddleSound.dispose();
    }
}