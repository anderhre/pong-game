package com.mygdx.game.states.sprites;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Vector2;
import com.mygdx.game.Pong;

public class Ball {
    private final Vector2 position;
    private final Vector2 velocity;
    private final Texture ball;

    private final static int MINSTARTVX = -5;  // -5 <= VoX <= 5 (extra rules at line 28)
    private final static int MINSTARTVY = -4;  // -4 <= VoY <= 4 (extra rules at line 31)
    public final static int MAXVX = 18;
    public final static int MAXVY = 10;
    public final static float VXINCREMENT = 0.5f;
    public final static float VYINCREMENT = 0.4f;

    public Ball() {
        ball = new Texture("Pongball.png");
        position = new Vector2(Pong.WIDTH / 2.0f - ball.getWidth() / 2.0f, Pong.HEIGHT / 2.0f + ball.getHeight() / 2.0f);
        velocity = generateStartVelocity();
    }

    public Vector2 generateStartVelocity() {
        int vx = 0;
        int vy = 0;

        // Randomly generate numbers between min and max (max = abs(min))
        while (vx >= - 3 && vx <= 3) {
            vx = MINSTARTVX + (int) (Math.random() * ((Math.abs(MINSTARTVX) - MINSTARTVX) + 1));
        }
        while (vy == 0) {
            vy = MINSTARTVY + (int) (Math.random() * ((Math.abs(MINSTARTVY) - MINSTARTVY) + 1));
        }
        return new Vector2(vx, vy);
    }

    public void reverseXVelocity() {
        velocity.x = -velocity.x;
        increaseVelocity();
    }

    public void reverseYVelocity() {
        velocity.y = -velocity.y;
    }

    public void increaseVelocity() {
        // Increase velocity on x-axis if Vx < 9
        if (Math.abs(velocity.x) < MAXVX) {
            if (velocity.x > 0) {
                velocity.x += VXINCREMENT;
            } else {
                velocity.x -= VXINCREMENT;
            }
        }
        // Increase velocity on y-axis if Vy < 8
        if (Math.abs(velocity.y) < MAXVY) {
            if (velocity.y > 0) {
                velocity.y += VYINCREMENT;
            } else {
                velocity.y -= VYINCREMENT;
            }
        }
    }

    public Vector2 getPosition() {
        return position;
    }

    public Vector2 getVelocity() {
        return velocity;
    }

    public Texture getTexture() {
        return ball;
    }

    public void update() {
        position.add(velocity.x, velocity.y);
    }

    public void dispose() {
        ball.dispose();
    }
}
