package com.mygdx.game.states.sprites;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Vector2;
import com.mygdx.game.Pong;

public class Paddle {
    private final Vector2 position;

    private final Texture paddle;

    public Paddle(int xPos) {
        paddle = new Texture("Pongpaddle.png");
        position = new Vector2(xPos - paddle.getWidth() / 2.0f, Pong.HEIGHT / 2.0f - paddle.getHeight() / 2.0f);
    }

    public void moveTo(float y) {
        position.y = y;
    }

    public Texture getTexture() {
        return paddle;
    }

    public Vector2 getPosition() {
        return position;
    }

    public void dispose() {
        paddle.dispose();
    }
}
