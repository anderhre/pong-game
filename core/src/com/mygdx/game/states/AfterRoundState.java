package com.mygdx.game.states;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.mygdx.game.Pong;
import com.mygdx.game.Singleton;

public class AfterRoundState extends State {

    private final GameStateManager gsm;

    private final BitmapFont leftPointsFont, rightPointsFont, playerWonFont, continueFont;

    private static final String LEFTPLAYERWONTEXT = "Left player won round!";
    private static final String RIGHTPLAYERWONTEXT = "Right player won round!";
    private final static String CONTINUETEXT = "Continue!";

    private final Texture board;
    private final Texture continueButton;

    public AfterRoundState(GameStateManager gsm) {
        super(gsm);

        this.gsm = gsm;

        leftPointsFont = new BitmapFont();
        leftPointsFont.getData().setScale(3);
        leftPointsFont.setColor(Color.YELLOW);
        rightPointsFont = new BitmapFont();
        rightPointsFont.getData().setScale(3);
        rightPointsFont.setColor(Color.YELLOW);

        playerWonFont = new BitmapFont();
        playerWonFont.getData().setScale(2);

        continueFont = new BitmapFont();
        continueFont.getData().setScale(2.5f);
        continueFont.setColor(Color.YELLOW);

        board = new Texture("PongboardWPaddles.png");
        continueButton = new Texture("Playbutton.png");
    }

    @Override
    protected void handleInput() {
        if(Gdx.input.justTouched() || Gdx.input.isKeyPressed(Input.Keys.ENTER) || Gdx.input.isKeyPressed(Input.Keys.SPACE)) {
            gsm.set(new PlayState(gsm));
        }
    }

    @Override
    public void update(float dt) {
        handleInput();
    }

    @Override
    public void render(SpriteBatch sb) {
        // Render board
        sb.draw(board, 0, 0);

        // Render points
        Integer leftPoints = Singleton.getInstance().leftPoints;
        Integer rightPoints = Singleton.getInstance().rightPoints;

        GlyphLayout layout = new GlyphLayout(leftPointsFont, leftPoints.toString());
        leftPointsFont.draw(sb, leftPoints.toString(), Pong.WIDTH / 2.0f - 40 - layout.width, Pong.HEIGHT - 28);
        rightPointsFont.draw(sb, rightPoints.toString(), Pong.WIDTH / 2.0f + 40, Pong.HEIGHT - 28);

        // Render winning player text
        if (Singleton.getInstance().leftPoints > Singleton.getInstance().rightPoints) {
            layout = new GlyphLayout(playerWonFont, LEFTPLAYERWONTEXT);
            playerWonFont.draw(sb, LEFTPLAYERWONTEXT, Pong.WIDTH / 2.0f - layout.width - 100, Pong.HEIGHT - 35);
        } else {
            playerWonFont.draw(sb, RIGHTPLAYERWONTEXT, Pong.WIDTH / 2.0f + 100, Pong.HEIGHT - 35);
        }

        // Render continue button and it's text
        sb.draw(continueButton, Pong.WIDTH / 2.0f - continueButton.getWidth() / 2.0f + 25, Pong.HEIGHT / 2.0f - continueButton.getHeight() / 2.0f);
        layout = new GlyphLayout(continueFont, CONTINUETEXT);
        continueFont.draw(sb, CONTINUETEXT, Pong.WIDTH / 2.0f - layout.width / 2.0f + 5, Pong.HEIGHT / 2.0f + layout.height / 2);
    }

    @Override
    public void dispose() {
        board.dispose();
        continueButton.dispose();
    }
}
