package com.mygdx.game.states;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.mygdx.game.Pong;

public class MenuState extends State {

    private final BitmapFont font;
    private final Texture playButton;
    private final Texture board;

    private final static String MENUTEXT = "Welcome to Pong!";
    private final static String PLAYTEXT = "Play!";

    public MenuState(GameStateManager gsm) {
        super(gsm);

        font = new BitmapFont();
        font.setColor(Color.YELLOW);
        font.getData().setScale(4);

        board = new Texture("PongboardWPaddles.png");
        playButton = new Texture("Playbutton.png");
    }

    @Override
    protected void handleInput() {
        if(Gdx.input.justTouched() || Gdx.input.isKeyPressed(Input.Keys.ENTER) || Gdx.input.isKeyPressed(Input.Keys.SPACE)) {
            gsm.set(new PlayState(gsm));
        }
    }

    @Override
    public void update(float dt) {
        handleInput();
    }

    @Override
    public void render(SpriteBatch sb) {
        // Render board
        sb.draw(board, 0, 0);

        // Render menutext
        GlyphLayout layout = new GlyphLayout(font, MENUTEXT);
        font.draw(sb, MENUTEXT, Pong.WIDTH / 2.0f - layout.width / 2.0f, Pong.HEIGHT / 2.0f + layout.height / 2 + 150);

        // Render playbutton and it's text
        sb.draw(playButton, Pong.WIDTH / 2.0f - playButton.getWidth() / 2.0f + 25, Pong.HEIGHT / 2.0f - playButton.getHeight() / 2.0f);
        layout = new GlyphLayout(font, PLAYTEXT);
        font.draw(sb, PLAYTEXT, Pong.WIDTH / 2.0f - layout.width / 2.0f, Pong.HEIGHT / 2.0f + layout.height / 2);
    }

    @Override
    public void dispose() {
        playButton.dispose();
        font.dispose();
    }
}
