package com.mygdx.game;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.mygdx.game.states.GameStateManager;
import com.mygdx.game.states.MenuState;

public class Pong extends ApplicationAdapter {
	public static final String TITLE = "Pong";
	public static final int WIDTH = 1000;
	public static final int HEIGHT = 650;
	public static final int WALLHEIGHT = 13;
	public static final int POINTSTOWIN = 5;

	private GameStateManager gsm;
	private SpriteBatch batch;
	
	@Override
	public void create () {
		gsm = new GameStateManager();
		gsm.push(new MenuState(gsm));
		batch = new SpriteBatch();
	}

	@Override
	public void render () {
		Gdx.gl.glClearColor(0, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

		Singleton.getInstance().camera.update();

		batch.begin();
		batch.setProjectionMatrix(Singleton.getInstance().camera.combined);
		gsm.update(Gdx.graphics.getDeltaTime());
		gsm.render(batch);
		batch.end();
	}

	@Override
	public void resize (int width, int height) {
		Singleton.getInstance().viewport.update(width, height);
	}

	@Override
	public void dispose () {
		batch.dispose();
	}
}
